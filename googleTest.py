import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from page.googleMainPage import MainPage
from page.googleSearchResultPage import SearchResultPage


class TestBase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('drivers/chromedriver')

    def go_to(self, url):
        self.driver.get(url)

    def press_enter(self, web_element):
        web_element.send_keys(Keys.RETURN)

    def input_text(self, web_element, txt):
        web_element.clear()
        web_element.send_keys(txt)

    def tearDown(self):
        self.driver.close()


class TestExtension(TestBase):
    def test_sample2(self):
        self.go_to("https://www.google.com/")
        request = "Python"
        main_page = MainPage(self.driver)
        search_bar = main_page.get_search_bar()
        self.input_text(search_bar, request)
        self.press_enter(search_bar)
        results_page = SearchResultPage(self.driver)
        for element in results_page.get_results():
            print(element.get_attribute("textContent"))


if __name__ == "__main__":
    unittest.main()

