import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class GoogleSearchTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('drivers/chromedriver.exe')

    # Testing Single Input Field.
    def test_singleInputField(self):
        pageUrl = "https://www.google.com/"
        self.driver.get(pageUrl)

        elem = self.driver.find_element_by_name("q")
        elem.send_keys("Python")
        elem.send_keys(Keys.RETURN)

        elements = self.driver.find_elements_by_class_name("LC20lb")
        self.assertTrue(len(elements) > 0)
        for element in elements:
            print(element.get_attribute("textContent"))

    # Closing the browser.
    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
