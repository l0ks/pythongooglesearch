import unittest

from selenium import webdriver
from page.googleMainPage import MainPage
from page.googleSearchResultPage import SearchResultPage
from tests.base import TestBaseCore


class Extension(TestBaseCore, unittest.TestCase):

    def setUp(self):
        super(Extension, self).setUp()

    def test_sample2(self):
        self.go_to("https://www.google.com/")
        main_page = MainPage(self.driver)
        search_bar = main_page.get_search_bar()
        self.input_text(search_bar, "Python")
        self.press_enter(search_bar)
        results_page = SearchResultPage(self.driver)
        for element in results_page.get_results():
            print(element.get_attribute("textContent"))


if __name__ == "__main__":
    unittest.main()
