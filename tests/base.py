import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class TestBaseCore(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('drivers/chromedriver')

    def go_to(self, url):
        self.driver.get(url)

    def press_enter(self, web_element):
        web_element.send_keys(Keys.RETURN)

    def input_text(self, web_element, txt):
        web_element.clear()
        web_element.send_keys(txt)

    def tearDown(self):
        self.driver.close()