from page.basePage import BasePage


class MainPage(BasePage):

    def get_search_bar(self):
        element = self.driver.find_element_by_name("q")
        return element
