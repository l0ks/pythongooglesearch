from page.basePage import BasePage


class SearchResultPage(BasePage):

    def get_results(self):
        elements = self.driver.find_elements_by_class_name("LC20lb")
        return elements
