import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from models.book import Book
from page.amazon.amazonBestseller import AmazonBestsellers
from page.amazon.amazonDetails import AmazonItemDetails

from page.amazon.amazonMain import AmazonMainPage
from page.amazon.amazonResult import AmazonSearchResults
from page.googleMainPage import MainPage
from page.googleSearchResultPage import SearchResultPage


class TestBase(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome('drivers/chromedriver')

    def go_to(self, url):
        self.driver.get(url)

    def click_on_elem(self, web_element):
        web_element.click()

    def input_text(self, web_element, txt):
        web_element.clear()
        web_element.send_keys(txt)

    def press_enter(self, web_element):
        web_element.send_keys(Keys.RETURN)

    def select_option(self, select_element, input_option):
        select = Select(select_element)
        select.select_by_visible_text(input_option)

    def tearDown(self):
        self.driver.close()


class AmazonTest(TestBase):
    main_url = "https://www.amazon.com/"
    bestsellers_url = "https://www.amazon.com/Best-Sellers-Kindle-Store-Computer-Programming/zgbs/digital-text" \
                      "/156140011 "
    book_request_url = "https://www.amazon.com/Head-First-Java-Kathy-Sierra/dp/0596009208/ref=sr_1_3"
    search_request = "Java"
    category = "Books"

    def get_books(self, result_page):
        books = []
        for element in result_page.get_results():
            book = Book(
                result_page.get_title(element),
                result_page.get_author(element),
                result_page.get_rating(element)
            )
            books.append(book)
        return books

    def compare_books_to_bestsellers(self, books, bestsellers_list):
        for bestseller in bestsellers_list:
            bestseller_title = bestseller.get_attribute("Title")
            for book in books:
                book.check_bestseller(bestseller_title)
        return books

    def search_for_book(self, books, requested_book):
        result = False
        for book in books:
            if book.title.find(requested_book.title) > -1:
                result = True
                break
        return result

    def test_sample2(self):
        self.go_to(self.main_url)

        # Main page
        amazon_main_page = AmazonMainPage(self.driver)

        search_bar = amazon_main_page.get_search_bar()
        self.input_text(search_bar, self.search_request)

        select_element = amazon_main_page.get_select()
        self.select_option(select_element, self.category)

        search_btn = amazon_main_page.get_search_button()
        self.click_on_elem(search_btn)

        # Search results
        amazon_search_results_page = AmazonSearchResults(self.driver)
        books = self.get_books(amazon_search_results_page)

        # Bestsellers

        self.go_to(self.bestsellers_url)

        amazon_bestsellers_page = AmazonBestsellers(self.driver)
        bestsellers_list = amazon_bestsellers_page.get_bestsellers()
        books = self.compare_books_to_bestsellers(books, bestsellers_list)

        for book in books:
            book.show()

        # Book request

        self.go_to(self.book_request_url)

        book_request_page = AmazonItemDetails(self.driver)

        search_result = self.search_for_book(books, book_request_page.get_book())
        print("Match result: ", search_result)


if __name__ == "__main__":
    unittest.main()
